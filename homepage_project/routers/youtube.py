from fastapi import (
    Depends,
    APIRouter,
)
from authenticator import authenticator
from models import YoutubeIn, YoutubeOut, YoutubesOut, Account
from queries.youtube import YoutubeQueries

router = APIRouter()

@router.post("/api/youtube", response_model=YoutubeOut)
async def create_youtube(
    info: YoutubeIn,
    account: Account = Depends(authenticator.get_current_account_data),
    youtubes: YoutubeQueries = Depends()
):
    youtube = youtubes.create(info, account["id"])
    return youtube

@router.get("/api/youtube", response_model=YoutubesOut)
async def get_youtube_videos_for_user(
    account: Account = Depends(authenticator.get_current_account_data),
    youtubes: YoutubeQueries = Depends()
):
    youtubes = youtubes.get(account["id"])
    return youtubes

@router.put("/api/youtube", response_model=YoutubeOut)
async def update_youtube(
    info: YoutubeOut,
    account: Account = Depends(authenticator.get_current_account_data),
    youtubes: YoutubeQueries = Depends()
):
    youtube = youtubes.update(info, account["id"])
    return youtube


@router.delete("/api/youtube", response_model=bool)
async def delete_youtube(
    id,
    account: Account = Depends(authenticator.get_current_account_data),
    youtubes: YoutubeQueries = Depends()
):
    youtube = youtubes.delete(id, account["id"])
    return youtube