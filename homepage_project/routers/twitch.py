from fastapi import (
    Depends,
    APIRouter,
)
from authenticator import authenticator
from models import TwitchIn, TwitchOut, TwitchsOut, Account
from queries.twitch import TwitchQueries

router = APIRouter()

@router.post("/api/twitch", response_model=TwitchOut)
async def create_twitch(
    info: TwitchIn,
    account: Account = Depends(authenticator.get_current_account_data),
    twitchs: TwitchQueries = Depends()
):
    twitch = twitchs.create(info, account["id"])
    return twitch

@router.get("/api/twitch", response_model=TwitchsOut)
async def get_twitch_channels(
    account: Account = Depends(authenticator.get_current_account_data),
    twitchs: TwitchQueries = Depends()
):
    twitchs = twitchs.get(account["id"])
    return twitchs


@router.put("/api/twitch", response_model=TwitchOut)
async def update_twitch(
    info: TwitchOut,
    account: Account = Depends(authenticator.get_current_account_data),
    twitchs: TwitchQueries = Depends()
):
    twitch = twitchs.update(info, account["id"])
    return twitch


@router.delete("/api/twitch", response_model=bool)
async def delete_twitch(
    id,
    account: Account = Depends(authenticator.get_current_account_data),
    twitchs: TwitchQueries = Depends()
):
    twitch = twitchs.delete(id, account["id"])
    return twitch