from .client import Queries
from models import TwitchIn, TwitchOut, TwitchsOut, Account
from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId

class TwitchQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "Twitch"

    def create(self, info: TwitchIn, account_id: str) -> TwitchOut:
        props = info.dict()
        props["user_id"] = account_id
        try:
            self.collection.insert_one(props)
        except DuplicateKeyError:
            raise DuplicateKeyError()
        props["id"] = str(props["_id"])
        return TwitchOut(**props)

    def get(self, account_id: str) -> TwitchsOut:
        channel_objects = self.collection.find( {"user_id":account_id} )
        channels = {"twitchs": []}
        for channel in channel_objects:
            channel["id"] = str(channel["_id"])
            channels["twitchs"].append(TwitchOut(**channel))
        print(channels)
        return TwitchsOut(**channels)
    
    def update(self, info: TwitchOut, account_id: str) -> TwitchOut: 
        props = info.dict()
        props["user_id"] = account_id
        try:
            self.collection.update_one(
                {"user_id": account_id,
                 "_id": ObjectId(props["id"])},
                {"$set": {
                "channel_name": props["channel_name"],
                "channel_id": props["channel_id"]
                }},
                False
            )
        except DuplicateKeyError:
            raise DuplicateKeyError
        return TwitchOut(**props)
    
    def delete(self, id: str, account_id: str) -> bool:
        self.collection.delete_one({"user_id": account_id, 
                                    "_id": ObjectId(id)})
        return True    
