from .client import Queries
from models import YoutubeIn, YoutubeOut, YoutubesOut, Account
from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId

class YoutubeQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "Youtube"

    def create(self, info: YoutubeIn, account_id: str) -> YoutubeOut:
        props = info.dict()
        props["user_id"] = account_id
        try:
            self.collection.insert_one(props)
        except DuplicateKeyError:
            raise DuplicateKeyError()
        props["id"] = str(props["_id"])
        return YoutubeOut(**props)

    def get(self, account_id: str) -> YoutubesOut:
        video_objects = self.collection.find( {"user_id":account_id} )
        videos = {"youtubes": []}
        for video in video_objects:
            video["id"] = str(video["_id"])
            videos["youtubes"].append(YoutubeOut(**video))
        return YoutubesOut(**videos)
    
    def update(self, info: YoutubeOut, account_id: str) -> YoutubeOut: 
        props = info.dict()
        props["user_id"] = account_id
        try:
            self.collection.update_one(
                {"user_id": account_id,
                 "_id": ObjectId(props["id"])},
                {"$set": {
                "channel_name": props["channel_name"],
                "channel_id": props["channel_id"]
                }},
                False
            )
        except DuplicateKeyError:
            raise DuplicateKeyError
        return YoutubeOut(**props)
    
    def delete(self, id: str, account_id: str) -> bool:
        self.collection.delete_one({"user_id": account_id, 
                                    "_id": ObjectId(id)})
        return True